# SDK Reference #

SDK provides "Forcecube" class, this is the only class which you should use.

Here are 2 methods which provides "Forcecube" class


```
#!objective-c

/**
 * Saves provided messageId into local Database
 * which will be further dispatched to server
 *
 * @param aId messageId which will be saved and dispatched to server
 */
+ (void)addMessageWithId:(NSNumber *)aId;

/**
 * Sets up Forcecube's infrastructure.
 *
 * - Authenticates SDK Key
 * - Starts Beacon Monitoring/Ranging
 * - Starts Event dispatching to server
 *
 * @param aSDKKey unique identifier which will be authenticated
 * @param aCallback will be invoked after successfull/failure start
 */
+ (void)startServiceWithSDKKey:(NSString *)aSDKKey
                    completion:(ForcecubeStartServiceCallback)aCallback;
```



# Steps to get set up #

1. Link dynamic libraries, that libForcecube uses
2. Add libForcecube static library to your project
3. Start FORCECUBE Service

## 1) Link dynamic libraries, that libForcecube uses ##

The List of dynamic libraries that FORCECUBE uses

* AdSupport.framework
* Foundation.framework
* CoreLocation.framework
* CoreTelephony.framework
* libsqlite3.dylib
* MobileCoreServices.framework
* SystemConfiguration.framework
* UIKit.framework

*(use Apple's guide for linking this libraries to your target https://developer.apple.com/library/ios/recipes/xcode_help-project_editor/Articles/AddingaLibrarytoaTarget.html)*

## 2) Add libForcecube static library to your project ##

***Step 1.***

Make sure you have *Forcecube* folder including 

* ForceCuBe.h
* ForceCuBeBundle.bundle
* libForcecube.a

![Screen Shot 2014-08-15 at 8.12.06 PM.png](https://bitbucket.org/repo/8AKdGR/images/1990574665-Screen%20Shot%202014-08-15%20at%208.12.06%20PM.png)

***Step 2.***

Drag *Forcecube* folder and drop it into Xcode's project structure.

![Screen Shot 2014-08-15 at 4.42.56 PM.png](https://bitbucket.org/repo/8AKdGR/images/2251909831-Screen%20Shot%202014-08-15%20at%204.42.56%20PM.png)

Xcode will alert you about adding new files into the project (Make sure "Copy items into destination group’s folder (if needed)" is checked) and press "Finish".


***Step 3.***

Verify that you got structure like this.

![Screen Shot 2014-08-15 at 8.14.15 PM.png](https://bitbucket.org/repo/8AKdGR/images/1397471348-Screen%20Shot%202014-08-15%20at%208.14.15%20PM.png)

## 3) Start FORCECUBE Service ##

***Use this code to start FORCECUBE Service.***
```
#!objective-c

    [Forcecube startServiceWithSDKKey:@"yourKeyHere"
                           completion:^(NSError *error) { 
    }];

```

***Use this code to add custom messages.***
```
#!objective-c
[Forcecube addMessageWithId:@1234];
```

***Example of usage***

![Screen Shot 2014-08-15 at 8.43.17 PM.png](https://bitbucket.org/repo/8AKdGR/images/515147418-Screen%20Shot%202014-08-15%20at%208.43.17%20PM.png)