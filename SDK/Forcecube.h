//
//  ForceCuBe.h
//  ForceCuBe
//
//  Created by David Sahakyan on 6/30/14.
//  Copyright (c) 2014 ForceCuBe. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^ForcecubeStartServiceCallback) (NSError *error);

@interface Forcecube : NSObject

/**
 * Saves provided messageId into local Database
 * which will be further dispatched to server
 *
 * @param anId messageId which will be saved and dispatched to server
 */
+ (void)addMessageWithId:(NSNumber *)anId;

/**
 * Sets up ForceCuBe's infrastructure.
 *
 * - Authenticates SDK Key
 * - Starts Beacon Monitoring/Ranging
 * - Starts Event dispatching to server
 *
 * @param aSDKKey unique identifier which will be authenticated
 *
 * @param aRSSI touch detection's RSSI threshold
 * If the RSSI value of the beacon will be greater that provided threshold
 * touch event will be generated, and saved into database, which
 * will be further dispatched to server
 *
 * @param aCallback will be invoked after successfull/failure start
 */
+ (void)startServiceWithSDKKey:(NSString *)aSDKKey
       touchDetectionThreshold:(NSInteger)aRSSI
                    completion:(ForcecubeStartServiceCallback)aCallback;

@end
